# Classifying audio signals of cat and dog sounds

- In this project, I used CNNs on visual images of audio spectrograms of cats and dogs to predict whether the noises made were cats or dogs

CAT:
![CAT](/non_pad_MFCC_train/cat/cat_10.jpeg)



DOG:
![DOG](/non_pad_MFCC_train/dog/dog_barking_10.jpeg)




## 1. Preprocesing

- Using the Generate_MFCCS.py file, I first preprocessed, standardized and generated mel spectrograms of the audio files
- This included a function that removed silence segnments in the clips ("envelope")
- Since each audio file was a different length, I needed to zero pad the spectrograms

## 2. CNN
- CNN_MFCC.py then took the raw audio spectrograms, and used a transform function from PyTorch to standardize them:
- Transform function looks like this:
![Transform-function](Figure_4.png)




- I used train and test loader to feed the CNN
- Here was the structure of my CNN:

class CNN(nn.Module):

    def __init__(self):

        super().__init__()
        self.conv1 = nn.Conv2d(3, 6, 3, 1)
        self.conv2 = nn.Conv2d(6, 16, 3, 1)
        self.fc1 = nn.Linear(61*61*16, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84,10)
        self.fc4 = nn.Linear(10,2)

    def forward(self, X):
       X = F.relu(self.conv1(X))
       X = F.max_pool2d(X,2,2)
       X = F.relu(self.conv2(X))
       X = F.max_pool2d(X,2,2)
       X = X.view(-1, 61*61*16)
       X = F.relu(self.fc1(X))
       X = F.relu(self.fc2(X))
       X = F.relu(self.fc3(X))
       X = self.fc4(X)
       return F.log_softmax(X, dim = 1

- CNN used Adam and cross entropy loss as the optimizer and loss function

- Model was 87% accurate on estimate of the test error.



